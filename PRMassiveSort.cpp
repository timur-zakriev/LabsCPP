#include <iostream>
#include <ctime>
using namespace std;

void randArrow(int numberMassive, int size, int arrow[]){ // заполняет массив случайными числами
  srand(time(NULL));
  cout << "Arrow " << numberMassive << " : { "; // красивый вывод массива
  for(int i = 0; i < size; i++){ // заполняет массив значениями
    arrow[i] = rand() % 200 - 100; // диапозон значений от -100 до 100
    cout << arrow[i] << " "; // распечатывает полученный массив
  }
  cout << "}" << endl;
}
void showArrow(int numberMassive, int size, int arrow[]){ // распечатывает массив
  cout << "modArrow " << numberMassive << " : { "; // все тот же красивый вывод
  for(int i = 0; i < size; i++){ // задает массив в N элементов
    cout << arrow[i] << " ";
  }
  cout << "}" << endl;
}
void choseSort(int size, int arrow[]){ // сортировка выбором
  int indmax, buffer;
  for (int i = 0; i < size - 1; i++){ // сортировка выбором по убиванию
    indmax = i;
    for (int j = i + 1; j < size; j++){
      if (arrow[j] > arrow[indmax]){
        indmax = j;
      }
    }
    buffer = arrow[i];
    arrow[i] = arrow[indmax];
    arrow[indmax] = buffer;
  }
}
void bubleSort(int size, int arrow[]){
  int buffer;
  for (int i = 0; i < size - 1; i++){ // сортировка пузырьком по возрастанию
    for (int j = 0; j < size - i - 1; j++){
      if (arrow[j] > arrow[j + 1]){
        buffer = arrow[j];
        arrow[j] = arrow[j + 1];
        arrow[j + 1] = buffer;
      }
    }
  }
}
int main(){
  int arrowOne[10], arrowTwo[15]; // задаем необходимые массивы
  randArrow(1, 10, arrowOne); // заполняем массивы
  randArrow(2, 15, arrowTwo);
  choseSort(10, arrowOne); // сортируем выбором первый массив
  bubleSort(15, arrowTwo); // сортируем пузырьком второй массив
  showArrow(1, 10, arrowOne); // выводим массивы
  showArrow(2, 15, arrowTwo);
  return 0;
}
