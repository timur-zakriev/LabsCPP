#include <iostream>
#include <cmath>
using namespace std;
int main() {
    setlocale(LC_ALL,"Russian");
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(2);

    double x1,x2,x3,y1,y2,y3,result,p,a,b,c;//обьявляет переменные
    cout<<"Вычисление площади треугольника.\nВвелите координаты углов\n(числа разделяйте пробелом):\n";
    cout<<"x1,y1=>";    cin>>x1>>y1;        //вводим координаты вершин треугольника
    cout<<"x2,y2=>";    cin>>x2>>y2;
    cout<<"x3,y3=>";    cin>>x3>>y3;
    a=sqrt(pow(x2-x1,2)+pow(y2-y1,2));
    b=sqrt(pow(x3-x1,2)+pow(y3-y1,2));
    c=sqrt(pow(x2-x3,2)+pow(y2-y3,2));
    p=(a+b+c)/2;
    result=sqrt(p*(p-a)*(p-b)*(p-c));       //финальная формула для нахождения площади
    cout<<"Площадь треугольника:"<<result<<"кв.см."<<endl;
    return 0;
}
