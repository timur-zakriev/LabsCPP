//Вариант 26
#include <iostream>
using namespace std;
int Cif(int K){//считает СУММУ чисел введенной последовательности
    int s=0;
    do {
        s+=K%10;
        K/=10;
    } while(K!=0);
    return s;
}
int col(int K){//считает КОЛИЧЕСТВО чисел введенной последовательности
    int g=0;
    do{
        g+=1;
        K/=10;
    }while(K!=0);
    return g;
}
int main(){
    int n,t=0,sumNew=0;
    cout<<"Input number=>";
    cin>>n;
    do {
        cout<<Cif(n);
        sumNew+=Cif(n);
        t++;
    } while(t!=col(n));
    cout<<endl<<sumNew<<endl;
    return 0;
}
