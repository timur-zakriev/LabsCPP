#include <iostream>
#include <cmath>
using namespace std;

double f1(double x){
    return 1/(x*sqrt(pow(x,2)-1));
}

double f2(double x){
    return pow(M_E,x)*pow(cos(x),2);
}

double middleRectangle(double a, double b, int col, double (*f)(double x)){
    double x = a, h = (b - a)/col, s = 0;
    for (int i = 1; i < col; i++){
        s += f(x+h/2);
        x += h;
    }
    return s * h;
}

int main(){
    cout << middleRectangle(-2,-1,1000,f1) << endl << middleRectangle(0,M_PI,200,f2) << endl;
    return 0;
}
