#include <iostream>
#include <cmath>
using namespace std;
int main(){
    //количество знаков после запятой
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(6);

    double y,xStart,xEnd,step;
    cout<<"Input xStart, xEnd, Step=>";
    cin>>xStart>>xEnd>>step;
    cout<<"X\t\t"<<"Y\n";
    for (double x = xStart; x < (xEnd+1); x=x+step) {
        if (x>=-7 && x<-3){
            cout<<x<<"\t"<<(3*x+21)/4<<endl;
        }
        if (x>=-3 && x<3){
            y=-sqrt(pow(3,2)-pow(x,2))+3;
            cout<<x<<"\t"<<y<<endl;
        }
        if (x>=3 && x<6){
            y=-2*x+9;
            cout<<x<<"\t"<<y<<endl;
        }
        if (x>=6 && x<=11){
            y=x-9;
            cout<<x<<"\t"<<y<<endl;
        }
    }
    cout<<"\nThe programm is end,milord"<<endl;
    return 0;
}
