#include <iostream>
#include <cmath>

using namespace std;
double element(double n){
    return 11.2*cos(2*n-1)+(abs(sin(1.5*n)))/1.7;
}
double composition(int n){
    if (n>1){
        return element(n)*composition(n-1);
    }
    else{
        return element(n);
    }
}
double no(int t){
  double s = 1;
  for (int n = 1; n <= t; n++){
    s *= element(n);
  }
  return s;
}
int main(){
    double s=1;
    cout<<composition(10)<<endl;
    cout<<no(10)<<endl;
    return 0;
}
