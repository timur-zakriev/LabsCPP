#include <iostream>
#include <cmath>
using namespace std;
int main() {
    double a,y,z;
    cout<<"Input a=>";
    cin>>a;
    y=pow(cos((3.0/8)*M_PI-a/4.0),2)-pow(cos((11.0/8)*M_PI+a/4.0),2);
    z=(sqrt(2)/2)*sin(a/2);
    cout<<"y="<<y<<"\nz="<<z<<endl;
    return 0;
}
