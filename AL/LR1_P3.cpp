//Вариант 9
#include <iostream>
#include <cmath>
using namespace std;

int main(){
    const double a=3,p=2,b=1;
    double x,y,f;
    cout<<"Input x,y=>"; cin>>x>>y;
    f=(x+(y/b))/(y-(x/a))*pow(cos(x/p),2)+(y-(x/a))/(x-(y/b))*pow(sin(y/p),2);
    cout<<"f(x,y)="<<f<<endl;
    return 0;
}
