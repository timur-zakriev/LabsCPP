// Вариант 9
#include <iostream>
#include <time.h>
#include <cstdlib>
using namespace std;
bool isZeroInMassive(int arrow[], int lenArrow){
  for (int i = 0; i < lenArrow; i++){
    if (arrow[i] == 0){
      return true;
    }
  }
  return false;
}
int main(){
  int lenArrow = 10, arrow[lenArrow];
  srand(time(NULL));
  for(int i = 0; i < lenArrow; i++){ // задает массив в 10 элементов
    arrow[i] = rand() % (10-(-10)+1)+(-10); // диапозон значений от -10 до 10
    cout<<arrow[i]<<" ";
  }

  int maxAbsNumber = 0, firstPositive = -1;

  for(int i = 0; i < lenArrow; i++){
    if ( maxAbsNumber < abs(arrow[i])){ // ищет максимальный по абсолютному значению элемент
      maxAbsNumber = abs(arrow[i]);
    }
    if (firstPositive == -1 && arrow[i] > 0){ // ищет первый положительный элемент
      firstPositive = i;
    }
  }

  cout << "\nmaxAbsNumber => " << maxAbsNumber << endl; // выводит значение максимального по абсолютному значению элемента в консоль

  int secondPositive = -1, amountElements = 0;
  for (int i = firstPositive + 1; i < lenArrow; i++){ // ищет второй положительный элемент массива
    if (secondPositive == -1 && arrow[i] > 0){
      secondPositive = i;
    }
  }

  if (firstPositive + 1 == secondPositive){ // обработка ошибки если между первым и вторым элементом ничего нет
    cout << "\nSory, milady, between firstPositive and secondPositive no elements.\n";
  }
  else{ // считает сумму элементов между первым положительным и последним
    for (int i = firstPositive + 1; i < secondPositive; i++){
      amountElements += arrow[i];
    }
    cout << "amountElements => " << amountElements << endl;
  }

  if (isZeroInMassive(arrow, lenArrow) == true){ // условие выполнения перемещения нулей
    int buffer;
    for (int i = 0; i < lenArrow; i++){ // перемещает нули в конец массива
      for (int j = i + 1; j < lenArrow; j++){
        if (arrow[j] != 0){
          buffer = arrow[j];
          arrow[j] = arrow[i];
          arrow[i] = buffer;
        }
      }
    }
  }
  else{ // обработка ошибки если в массиве нет нулей
    cout<<"Sory, milady, no zero in massive.\n";
  }

  for (int i = 0; i < lenArrow; i++){ // выводит полученный массив
    cout << arrow[i] << " ";
  }

  cout << endl;

  return 0;
}
