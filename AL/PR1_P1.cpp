#include <iostream>
using namespace std;
int main() {
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(3);

    double one=1.0,two=1.414,three=1.732,four=2.0,five=2.236;
    cout<<"N\t"<<"Square Root\n"<<"1\t"<<one<<"\n2\t"<<two<<"\n3\t"<<three<<"\n4\t"<<four<<"\n5\t"<<five<<endl;

    return 0;
}
