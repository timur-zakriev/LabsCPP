#include <iostream>
#include <cmath>
using namespace std;
int main(){
    //количество знаков после запятой
    cout.setf(ios::fixed);
    cout.setf(ios::showpoint);
    cout.precision(6);

    double y,xStart,xEnd,step;
    cout<<"Input xStart, xEnd, Step=>";
    cin>>xStart>>xEnd>>step;
    cout<<"X\t\t"<<"Y\n";
    for (double x = xStart; x < (xEnd+1); x=x+step) {
        if (x>=-9 && x<-7){
            y=0;
            cout<<x<<"\t"<<y<<endl;
        }
        else if (x>=-7 && x<-3){
            y=((x+7)/5)*4;
            cout<<x<<"\t"<<y<<endl;
        }
        else if (x>=-3 && x<-2){
            y=4;
            cout<<x<<"\t"<<y<<endl;
        }
        else if (x>=-2 && x<=2){
            y=pow(x,2);
            cout<<x<<"\t"<<y<<endl;
        }
        else if (x>=2 && x<4){
            y=((x-4)/-2)*4;
            cout<<x<<"\t"<<y<<endl;
        }
        else if (x>=4 && x<=7){
            y=0;
            cout<<x<<"\t"<<y<<endl;
        }
    }
    cout<<"\nThe programm is end,milady"<<endl;
    return 0;
}
