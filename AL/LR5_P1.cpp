#include <iostream>
using namespace std;
int main(){
    int number=1,numSeven=0,numSum=0,numNegative=0;
    while(number!=0){
        cout<<"Input number=>"; cin>>number;
        if (number>0 && number==7){                 //считает кол-во введенныч чисел кратных 7
            numSeven+=1;
        }
        else if(number>0 && number!=5){             //считает кол-во введеных положительных чисел не кратных 5
            numSum+=1;
        }
        else if(number<0){                          //считает сумму отрицательных чисел
            numNegative+=number;
        }
        else if(number==0){                         //обрывает цикл если введен 0
            break;
        }
        else if(number==5){                         //пропускает итерацию цикла если число равно 5
            continue;
        }
        else{                                       //выдает сообщение о неправильном вводе
            cout<<"Not incorrect input,milady.\n";
        }
    }
    cout<<"Quantity seven="<<numSeven<<"\nQuantity NOT five="<<numSum<<"\nAmount negative="<<numNegative<<endl;
    return 0;
}
