#include <iostream>
#include <cmath>
using namespace std;

long double fact(int N){ // вычисляет факториал
  if(N < 0) return 0;
  if(N == 0) return 1;
  else return N * fact(N - 1);
}

float a(float n){ // вычисляет по формуле
  return pow(2, n) / fact((n - 1));
}

int main(){
  int N = 4;
  float e = 0.0001;

  while(true){ // поиск значения
    if(abs(a(N) - a(N - 1)) <= e){ // вывод значений если они найдены
      cout << "a(n) = " << a(N) << endl;
      cout << "a(n - 1) = " << a(N - 1) << endl;
      cout << "n = " << N << endl;
      // cout << abs(a(N) - a(N - 1)) << endl;
      break;
    }
    N++;
  }

  return 0;
}
